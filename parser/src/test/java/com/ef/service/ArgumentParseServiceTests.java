package com.ef.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ArgumentParseServiceTests {

	UserArgService argumentService;

	@Before
	public void setup() {
		argumentService = new UserArgService();
	}

	@Test
	public void canCreateArgumentService() {
		assertNotNull(argumentService);
	}

	@Test
	public void junit4SampleTest() {
		assertTrue(true);
	}

	@Test(expected = NullPointerException.class)
	public void argsNullReturnsException() {
		Object result = argumentService.parseUserConfiguration(null);
//		assertNotNull(result);
	}
}
