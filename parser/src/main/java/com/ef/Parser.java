package com.ef;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ef.model.UserArgument;
import com.ef.service.UserArgService;
import com.ef.service.LogService;

@SpringBootApplication
public class Parser implements ApplicationRunner {
	public static final Logger logger = LoggerFactory.getLogger(ApplicationRunner.class);

	@Autowired
	private UserArgService apService;
	
	@Autowired
	private LogService logService;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Parser.class);
		app.run(args);
	}

	@Override
	public void run(ApplicationArguments arguments) throws Exception {
		try {
			UserArgument input = apService.parseUserConfiguration(arguments);
			logger.info("User arguments validated...");

			List<String> ips = logService.parse(input);
			logger.info("blocked IPs are listed...");

//			List<String> ips = logService.parse(input);
//			for (String string : ips) {
//				System.out.println(string);
//			}
			printResults(ips);
		} catch (IllegalArgumentException ex) {
			System.out.println(ex.getMessage());
		}

//        exit(0);
	}

	private void printResults(List<String> ips) {
		logger.info("The following IPS were blocked and updated to the database");
		ips.forEach(System.out::println);
	}
}
