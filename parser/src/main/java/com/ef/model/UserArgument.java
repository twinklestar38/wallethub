package com.ef.model;

import java.io.File;
import java.util.Date;

import lombok.Data;

@Data
public class UserArgument {
	private File logLocation;
	private Date startDate;
	private String duration;
	private Long threshold;
	private Date endDate;
}
