package com.ef.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

// Helper  class for operations 
public class Helper {

	private Helper() {
		// don't allow to instantiate this class.
	}

	public static Date parseInputDate(String date) {

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
			return format.parse(date);

		} catch (ParseException e) {
			throw new IllegalArgumentException(" Only the 'yyyy-MM-dd.HH:mm:ss' format is supported");
		}
	}

	public static Date addHour(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, 1);
		return cal.getTime();
	}

	public static Date addDay(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		return cal.getTime();
	}
}
