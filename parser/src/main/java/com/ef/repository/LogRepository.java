package com.ef.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ef.pojo.DummyPojo;

@Repository
public interface LogRepository extends CrudRepository<DummyPojo, Integer> {

	@Modifying
	@Transactional
	@Query(value = "TRUNCATE TABLE log", nativeQuery = true)
	public void cleanLogTable();

//	@Modifying
//	@Transactional
//	@Query(value = "DROP INDEX   IX_IP ON log", nativeQuery = true)
//	public void cleanLogTableIndex();

	@Modifying
	@Transactional
	@Query(value = "TRUNCATE TABLE blocked_ip", nativeQuery = true)
	public void cleanBlockedIpTable();

	@Modifying
	@Transactional
	@Query(value = "LOAD DATA  INFILE  ?1 " + " IGNORE INTO TABLE   log  " + " character set 'latin1' "
			+ " FIELDS  TERMINATED BY '|' " + " OPTIONALLY ENCLOSED BY '\"' " + " LINES TERMINATED BY '\\r\\n'"
			+ " (access_date,@ip,request,status,user_agent,@dummy) \r\n"
			+ " SET  ip=INET_ATON(@ip)", nativeQuery = true)
	public void bulkLoadData(String filePath);

	@Modifying
	@Transactional
	@Query(value = "CREATE INDEX IX_IP ON log(ip)", nativeQuery = true)
	public void createLogTableIndex();

	@Modifying
	@Transactional
	@Query(value = "CALL SP_GENERATE_BLOCKED_IP(?1,?2,?3)", nativeQuery = true)
	public List<String> generateBlockedIpList(Date startDate, Date endDate, Long threshold);

//	@Query(value = " SELECT  id,IP,NUM_OF_ACCESS, BLOCKING_COMMEN FROM blocked_ip", nativeQuery = true)
//	public List<BlockedIp> getBlockedIpList();
}
